import "normalize.css";
import "css/styles.css";
import React from "react";
import { render } from "react-dom";
import Application from "app/application";
import { Container } from "elements/container";

render(<Container><Application /></Container>, document.querySelector('.app-container'));