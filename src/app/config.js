export default class Config {
	static get API_URL() {
		return 'http://api.openweathermap.org/data/2.5/weather';
	}
	static get API_KEY() {
		return '5f62ec2986558ea3309423181ea235bb';
	}
	static get API_UNITS() {
		return 'metric';
	}
	static get API_LANG() {
		return 'ru'
	}
	static get RETRY_WAIT() {
		return 60000;
	}
	static get REFRESH_WAIT() {
		return 7200000;
	}
}