import React from 'react';
import autoBind from 'react-autobind';
import { FlexRow } from "elements/flex_grid";
import { Input, Button, Error } from "elements/ui";
import CitiesList from "components/cities_list";
import {Box, Heading, CityInputCell, CitySubmitCell, ModalTextError } from "elements/box";
import Modal from 'react-responsive-modal';
import Storage from "services/storage";
import Source from "services/source";
import Config from "app/config";

export default class Application extends React.Component {

	constructor(props) {
		super(props);

		this.emptyCityInfo = {
			temperature: null,
			pressure: null,
			notExists: false
		};
		this.cityInput = null;
		this.delay = {};

		const cities = {};
		Storage.loadList().forEach((title) => {
			const key = title.toLowerCase();
			cities[key] = Object.assign({title}, this.emptyCityInfo);
		});

		this.state = {
			error: '',
			modalText: '',
			modalOpen: false,
			cities
		};

		autoBind(this);
	}

	componentDidCatch() {
		this.setState({
			error: 'Application error. See console.'
		});
	}

	componentDidMount() {
		this.retrieveSourceInfo();
	}

	removeCity(cityName) {
		const { [cityName]: removedValue, ...citiesEdited } = this.state.cities;
		this.saveCitiesList(citiesEdited);
		this.setState({
			cities: citiesEdited
		})
	}

	saveCitiesList(list) {
		const saveList = Object.keys(list).map(key => list[key].title)
		Storage.saveList(saveList);
	}

	addCity() {
		const title = this.cityInput.value.trim();
		const listKey = title.toLowerCase();
		const { cities } = this.state;
		if (title) {
			if (~Object.keys(cities).indexOf(listKey)) {
				this.openModal(`"${title}" already in list`);
			} else {
				const entry = Object.assign({title}, this.emptyCityInfo);
				const newCitiesList = this.setCityInfo(listKey, entry, () => {
					this.saveCitiesList(newCitiesList);
					this.cityInput.value = '';
					this.retrieveCityInfo(listKey);
				});
			}
		} else {
			this.openModal('City name required');
		}
	}

	setCityInfo(city, values, callback) {
		const { cities } = this.state;
		const newCitiesList  = Object.assign({}, cities);
		newCitiesList[city] = Object.assign(cities[city] || {}, values);
		this.setState({
			cities: newCitiesList
		}, callback);
		return newCitiesList;
	}

	openModal(message) {
		this.setState({
			modalText: message,
			modalOpen: true
		});
	}

	modalClose() {
		this.setState({
			modalOpen: false
		});
	}

	retrieveSourceInfo() {
		const { cities } = this.state;
		Object.keys(cities).forEach((city) => {
			const info = cities[city];
			if (info.temperature == null) {
				this.retrieveCityInfo(city);
			}
		});
	}

	retrieveCityInfo(city) {
		if (this.delay[city]) {
			clearTimeout(this.delay[city]);
		}

		const { cities } = this.state;
		if (!cities[city]) return;

		const info = cities[city];
		if (info.notExists) return;

		this.delay[city] = setTimeout(() => {
			const sourceRequest = Source.retrieveInfo(city);
			sourceRequest.then((response) => {
				if (response.cod && response.cod === 200 && response.main) {
					const {temp: temperature, pressure} = response.main;
					this.setCityInfo(city, {
						temperature,
						pressure
					});
					this.refreshCityInfo(city);
					return;
				}
				this.retryRetrievingCityInfo(city);
			})
			.fail((xhr) => {
				if (xhr.status === 404) {
					this.setCityInfo(city, {
						temperature: null,
						pressure: null,
						notExists: true
					});
					return;
				}
				console.log('Error while retrieving weather info (' + city + '): ');
				console.log(xhr);
				console.log(' ');
				this.retryRetrievingCityInfo(city);
			});
		}, 100);
	}

	retryRetrievingCityInfo(city, delay) {
		delay = delay || Config.RETRY_WAIT;
		clearTimeout(this.delay[city]);
		this.delay[city] = setTimeout(() => {
			this.retrieveCityInfo(city)
		}, delay);
	}

	refreshCityInfo(city) {
		this.retryRetrievingCityInfo(city, Config.REFRESH_WAIT);
	}

	inputKeyDown(event) {
		if(event.key === 'Enter' && this.cityInput.value.trim()) this.addCity();
	}

	render() {
		const { error, cities, modalText, modalOpen } = this.state;
		if (error) return <Box><Error>{error}</Error></Box>

		return <Box>
			<Heading>OpenWeatherMap.org</Heading>
			<FlexRow>
				<CityInputCell>
					<Input placeholder="Enter city..." type="text" onKeyDown={ this.inputKeyDown } innerRef={e => { this.cityInput = e }} />
				</CityInputCell>
				<CitySubmitCell>
					<Button onClick={this.addCity}>Add</Button>
				</CitySubmitCell>
			</FlexRow>
			<CitiesList deleteCallback={this.removeCity} cities={cities} />
			<Modal open={modalOpen} classNames={{overlay:'modal-overlay', modal: 'modal-window'}} onClose={this.modalClose} center>
				<ModalTextError>{modalText}</ModalTextError>
			</Modal>
		</Box>;
	}

}