import reqwest from "reqwest";
import Config from "app/config";

export default class Source {

	static retrieveInfo(city) {
		const params = Source.getParams();
		params.data.q = city;
		return reqwest(params);
	}

	static getParams() {
		return {
			url: Config.API_URL,
			method: 'get',
			crossOrigin: true,
			data: {
				APPID: Config.API_KEY,
				units: Config.API_UNITS,
				lang: Config.API_LANG
			}
		}
	}

}