import Storages from "js-storage";

export default class Storage {

	static loadList() {

		let stored = [];
		const key = Storage.getKey();
		const storageProvider = Storage.getProvider();
		if (storageProvider.isSet(key)) {
			const storedValue = storageProvider.get(key);
			if (storedValue.constructor === Array) {
				stored = Storage.filterList(storedValue);
			}
		}
		return stored;
	}

	static saveList(value) {
		const key = Storage.getKey();
		const storageProvider = Storage.getProvider();
		if (value.constructor !== Array) {
			value = [value];
		}
		storageProvider.set(key, Storage.filterList(value));
	}

	static filterList(list) {
		return list.filter(word => (typeof word === 'string' || word instanceof String));
	}

	static getProvider() {
		return Storages.sessionStorage;
	}

	static getKey() {
		return 'weather_cities_list';
	}

}