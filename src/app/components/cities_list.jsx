import React from 'react';
import autoBind from 'react-autobind';
import {FlexHeading, CellCity, CellInfo, CellActions, Note} from "elements/list";
import CityInfo from "components/city_info";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import { faSortAlphaDown, faSortAlphaUp } from "@fortawesome/fontawesome-free-solid";

export default class CitiesList extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			order: this.props.order || 'asc'
		};
		autoBind(this);
	}

	changeOrder() {
		this.setState({
			order: this.state.order === 'asc' ? 'desc' : 'asc'
		});
	}

	render() {
		const { cities, deleteCallback } = this.props;
		const { order } = this.state;
		const cityNames = Object.keys(cities);

		if (!cityNames.length) return <Note>List is empty...</Note>;

		cityNames.sort();
		if (order === 'desc') {
			cityNames.reverse();
		}

		return [
			<FlexHeading key = {`0-${order}`}>
				<CellCity onClick={this.changeOrder}>City <FontAwesomeIcon icon={ order === 'asc' ? faSortAlphaDown : faSortAlphaUp } /></CellCity>
				<CellInfo>Temperature</CellInfo>
				<CellInfo>Pressure</CellInfo>
				<CellActions />
			</FlexHeading>,
			...cityNames.map((cityName) => {
				const info = cities[cityName];
				const { pressure, temperature, notExists} = info;
				const key = `${cityName}-${pressure}-${temperature}-${notExists}`;
				return <CityInfo
					key={key}
					cityName={cityName}
					deleteCallback={deleteCallback}
					cityInfo={info}
				/>;
			})
		];
	}
}