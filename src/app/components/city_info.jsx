import React from 'react';
import autoBind from 'react-autobind';
import {FlexRowHovered, CellCity, CellInfo, CellActions, RemoveTrigger, DimContainer, ErrorContainer, Temperature, Pressure } from "elements/list";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import { faTimes, faSpinner, faFrown} from "@fortawesome/fontawesome-free-solid";

export default class CityInfo extends React.Component {

	constructor(props) {
		super(props);
		autoBind(this);
	}

	render() {
		const { deleteCallback, cityName } = this.props;
		const { temperature, pressure, notExists, title } = this.props.cityInfo;
		return <FlexRowHovered>
			<CellCity>{
				notExists ?
					<ErrorContainer>{title} <small>(information not available)</small></ErrorContainer>	 :
					title
			}</CellCity>
			<CellInfo>{
				notExists ?
					<ErrorContainer><FontAwesomeIcon icon={faFrown} /></ErrorContainer> :
					(
						temperature ?
							<Temperature>{temperature}</Temperature> :
							<DimContainer><FontAwesomeIcon icon="spinner" pulse /></DimContainer>
					)
			}</CellInfo>
			<CellInfo>{
				notExists ?
					<ErrorContainer><FontAwesomeIcon icon={faFrown} /></ErrorContainer> :
					( pressure ?
						<Pressure>{pressure}</Pressure> :
						null
					)
			}</CellInfo>
			<CellActions>
				<RemoveTrigger title="Remove" onClick={() => {deleteCallback(cityName)}}>
					<FontAwesomeIcon icon={faTimes} />
				</RemoveTrigger>
			</CellActions>
		</FlexRowHovered>
	}
}