import React from "react";
import styled from "styled-components";

export const FlexRow = styled.div`
	display: flex;
	display: -webkit-flex;
	flex-wrap: nowrap;
	flex-direction: row;
	justify-content: flex-start;
	align-items: stretch;
	align-content: center;
`;

export const FlexCell = styled.div`
	flex: 1 1 auto;
	padding: 10px;
`;