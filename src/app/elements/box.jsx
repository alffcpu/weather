import React from "react";
import styled from "styled-components";
import {FlexCell} from "elements/flex_grid";

export const Box = styled.div`
	margin: 40px 0;
	min-width: 768px;
	width: 1024px;
	flex: 0 0 auto;
	padding: 20px;
	border: 1px solid #eee;
	box-shadow: 0 2px 5px 3px rgba(0,0,0,0.05);
	background-color: #fff;
	
	@media (max-width: 1064px) {
		width: 95%;
	}
`;

export const Heading = styled.h2`
	font-size: 24px;
	line-height: 24px;
	margin: 0 0 5px 0;
	padding: 0 0 5px 0;
	border-bottom: 1px solid #eee;
	text-align: center;
	font-weight: 300;
`;

export const CityInputCell = FlexCell.extend`
	width: 80%;
`;
export const CitySubmitCell = FlexCell.extend`
	width: 20%;
`;
export const ModalText = styled.p`
	width: 100%;
	margin: 0 40px 0 0;
	font-size: 18px;
`;
export const ModalTextError = ModalText.extend`
	color: #d00;
`;