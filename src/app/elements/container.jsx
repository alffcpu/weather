import React from "react";
import styled from "styled-components";

export const Container = styled.div`
	width: 100%;
	height: 100%;
	min-height: 100%;
	display: flex;
	display: -webkit-flex;
	flex-wrap: wrap;
	flex-direction: row;
	justify-content: space-around;
	align-items: center;
	align-content: center;
`;