import React from 'react';
import styled from "styled-components";
import {FlexCell, FlexRow} from "elements/flex_grid";

export const FlexHeading = FlexRow.extend`
	font-weight: bold;
	
	& > * {
		background-color: rgba(255,159,53,0.1);
		border-left-color: rgba(255,159,53,0.1) !important;
	}
	
	& > div:first-child {
		cursor: pointer;
	}
	
	& > div:first-child > * {
		margin-left: 10px;
		opacity: 0.5;
	}
	
	& > div:first-child:hover {
		text-decoration: underline;
	}
`;

export const FlexRowHovered = FlexRow.extend`			
	& > * {
		transition: all 250ms ease-in-out;
	}
	&:hover > * {
		background-color: #f0f0f0;
	}
`;

export const CellCity = FlexCell.extend`
	width: 50%;
	font-size: 18px;
	line-height: 24px;
	border-bottom: 1px solid #eee;
	min-height: 45px;
`;

export const CellInfo = CellCity.extend`
	width: 20%;
	font-weight: 700;
	text-align: center;
	border-left: 1px solid #eee;
`;

export const CellActions = CellInfo.extend`
	width: 10%;
`;

export const RemoveTrigger = styled.span`
	cursor: pointer;
	transform: scale(1);
	
	& > * {
		color: #d00;
		transition: all 150ms ease-in-out;
	}
	&:hover > * {
		color: #f00;
		transform: scale(1.4);
	}
`;

export const DimContainer = styled.span`
	& > * {
		opacity: 0.3;
	}
`;

export const Note = styled.div`
	margin: 20px 0;
	color: #999;
	font-weight: 300;
	letter-spacing: 2px;
	font-size: 14px;
	text-align: center;
	text-transform: uppercase;
`;

export const ErrorContainer = styled.span`
	color: #f00;
	opacity: 0.4;
	
	& > small {
		font-size: 10px;
		color: #666;
		white-space: nowrap;
	}
`;

export const Temperature = styled.span`
	&:after {
		content: "\00B0С";
		font-weight: 300; 
	}
`;

export const Pressure = styled.span`
	&:after {
		content: "hPa";
		font-weight: 300; 
		display: inline-block;
		margin-left: 2px;
	}
`;