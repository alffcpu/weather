import React from "react";
import styled from "styled-components";

export const Input = styled.input`
	box-shadow: 0;
	border: 1px solid #ccc;
	padding: 6px 10px;
	font-size: 14px;
	line-height: 18px;
	border-radius: 3px;
	width: 100%;
	
	&:focus {
		 border-color: #666;
		 box-shadow: 0 0 4px 2px rgba(0,0,0,0.03);
	}
`;

export const Button = styled.button`
	box-shadow: 0;
	border: 1px solid #ccc;
	padding: 6px 10px;
	font-size: 14px;
	line-height: 18px;
	border-radius: 3px;
	width: 100%;
	background-color: #f8f8f;
	cursor: pointer;
	
	&:hover, &:focus {
		 box-shadow: 0 0 4px 2px rgba(0,0,0,0.03);
		 background-color: #f0f0f0;
	}
`;

export const Error = styled.div`
	width: 100%;
	line-height: 28px;
	font-size: 24px;
	font-weight: 400;
	tex-align: center;
	padding: 10px;
	color: #e00;
`;